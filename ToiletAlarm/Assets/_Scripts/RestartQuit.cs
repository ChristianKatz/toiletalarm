﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//the script used to restart and stop the game 
namespace Toilette
{
    public class RestartQuit : MonoBehaviour
    {
        // that is the scene name for the restart
        [SerializeField]
        private string sceneName;

        // need this script to know when the game is over to let the player decides whether he wants to leave or to restart the game
        [SerializeField]
        private WinCondition winCondition;

        // the method for the button to restart the game
        public void RestartButton()
        {
            if(winCondition.Finished == true)
            {
                SceneManager.LoadScene(sceneName);
            }
        }

         // the method for a button to quit the game
         public void QuitGame()
         {
            if(winCondition.Finished == true)
            {
                Debug.Log("yes");
                Application.Quit();
            }
        }
    }
}