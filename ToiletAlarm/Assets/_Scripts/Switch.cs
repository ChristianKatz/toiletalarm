﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 // this script is not by me but I commentated it

// this script is for the switch that changes the gravity
public class Switch : MonoBehaviour
{
    // the value that contains the joint angle
    float previous = 0;

    // the hinge joint that is the switch
    HingeJoint joint;

    // the value that lower the gravaity in the game
    public float lowGravity;

    // the value that normalizes the gravity again
    public float normalGravity;

    private void Start()
    {
        // get the component
        joint = GetComponent<HingeJoint>();
    }

    private void FixedUpdate()
    {
        // if the switch was moved up or down, the gravity will change
        if(!Mathf.Approximately(previous, joint.angle))
        {
            if(joint.angle >= joint.limits.max)
            {
                Physics.gravity = new Vector3(0, lowGravity, 0);
            }
            else if(joint.angle <= joint.limits.min)
            {
                Physics.gravity = new Vector3(0, normalGravity, 0);
            }
        }

        previous = joint.angle;
    }
}
