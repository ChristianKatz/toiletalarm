﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// script that decides who has won
namespace Toilette
{
    public class WinCondition : MonoBehaviour
    {
        // timer of the game
        private float timer = 2;

        // Win picture if the player has survived the 2 minutes 
        [SerializeField]
        private GameObject WinPicture;

        // loser Picture if the player hasn't survived the 2 minutes
        [SerializeField]
        private GameObject LosePicture;

        // the text for the time
        [SerializeField]
        private TextMeshProUGUI timeText;

        // the text that shows up in the end so that the player can decide whether he wants to leave or stay in the game
        [SerializeField]
        private GameObject end;

        // variable that stores the enemy
        private Enemy[] enemy;

        // script that manages the flash light
        private Flash flash;

        // variable, that show if the game is won or not
        private bool finished = false;
        public bool Finished
        {
            get
            {
                return finished;
            }
            set
            {
                finished = value;
            }
        }

        void Start()
        {
            // get the scripts
            enemy = FindObjectsOfType<Enemy>();
            flash = FindObjectOfType<Flash>();

            // that will only show up if the enemy or the player has won the game
            WinPicture.SetActive(false);
            LosePicture.SetActive(false);
        }

        void Update()
        {
            // the calculation of the timer
            timer -= Time.deltaTime;

            // convert the float into a text value
            timeText.text = string.Format("Time left: {0:0}", timer);

            // the method that decides who has won and who has not
            GameCondition();
        }

        void GameCondition()
        {
            // if the player survived 2 minutes, the winning picture appears
            if (timer <= 0)
            {
                finished = true;
                WinPicture.SetActive(true);
                end.SetActive(true);
                Time.timeScale = 0;
            }

            // if the paparazzi takes a photo of the player, the loser pictured shows up
            if(timer > 0 && enemy[0].EnemyHasWon == true || enemy[1].EnemyHasWon == true || enemy[2].EnemyHasWon == true)
            {
                finished = true;
                LosePicture.SetActive(true);
                end.SetActive(true);
                flash.doCameraFlash = true;
                timer = 120;
            }
        }
    }
}