﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script is not by me but I commentated it


// this script is for the spawn of the toilet paper
namespace Toilette
{
    public class Dispenser : MonoBehaviour
    {
        // variables for the left and right prefabs to take and throw the toilet paper
        [SerializeField]
        private GameObject rightToiletRoll;
        [SerializeField]
        private GameObject leftToiletRoll;

        // the tag for right side, where a heap of toilet paper is
        [SerializeField]
        private string rightToiletRollTag;

        // the location where the right toilet paper spawns
        [SerializeField]
        private GameObject rightSpawn;

        // the location where the left toilet paper spawns
        [SerializeField]
        private GameObject leftSpawn;

        // the tag for right side, where a heap of toilet paper is
        [SerializeField]
        private string leftToiletRollTag;

        // varibale to count the right side of the toilet paper
        private int rightcount;

        // varibale to count the left side of the toilet paper
        private int leftcount;

        // set the number of toilet paper on the right side
        [SerializeField]
        private int ToiletRollNumberRight;

        // set the number of toilet paper on the left side
        [SerializeField]
        private int ToiletRollNumberLeft;


        void Start()
        {
            // spawn the toilet paper on the right side of the toilet
            Instantiate(rightToiletRoll, rightSpawn.transform.position, Quaternion.identity);

            // spawn the toilet paper on the right side of the toilet
            Instantiate(leftToiletRoll, leftSpawn.transform.position, Quaternion.identity);
        }

        void Update()
        {
            // count the toilet paper on the right side in the scene
            rightcount = GameObject.FindGameObjectsWithTag(rightToiletRollTag).Length;

            // count the toilet paper on the left side in the scene
            leftcount = GameObject.FindGameObjectsWithTag(leftToiletRollTag).Length;

            // When the toilet paper reaches the set number, the spawn stops
            if (rightcount < ToiletRollNumberRight)
            {
                Instantiate(rightToiletRoll, rightSpawn.transform.position, Quaternion.identity);
            }

            // When the toilet paper reaches the set number, the spawn stops
            if (leftcount < ToiletRollNumberLeft)
            {
                Instantiate(leftToiletRoll, leftSpawn.transform.position, Quaternion.identity);
            }

        }
    }
}