﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script is not by me but I commentated it

// this script disables the toilet paper that was thrown
namespace Toilette
{
    public class ToiletPaperDespawn : MonoBehaviour
    {
        // when the toilet paper comes out of the trigger box the seconds will begin to count for the respawn
        [SerializeField]
        private float secondsTillDespawn;

        // the tag of the despawn wall
        [SerializeField]
        private string despawnWall;

        // the bool value that decides when to enable the timer
        private bool goAhead;

        // the trigger box that triggers the countdown for the respawn
        public void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(despawnWall))
            {
                EnableDespawnTimer();
            }
        }

        void Start()
        {
            // at the start of the game the value is deactivated because the despawner shouldn't be running 
            goAhead = false;
        }

        // the method that starts the despawner
        void EnableDespawnTimer()
        {
            goAhead = true;
        }

        // the method that counts the seconds for the despawner
        void DespawnTimer()
        {
            secondsTillDespawn -= Time.deltaTime;
        }

        void Update()
        {
            // if the toilet paper leaves the trigger box, the timer for the despawn will start
            if (goAhead == true)
            {
                DespawnTimer();
            }
            // if the timer reaches 0, the object will be destroyed
            if (secondsTillDespawn <= 0)
            {
                Destroy(gameObject);
            }
        }
    }
}