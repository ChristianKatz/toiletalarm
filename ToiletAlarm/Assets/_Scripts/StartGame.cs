﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// the script with starts the game
namespace Toilette
{
    public class StartGame : MonoBehaviour
    {
        // variable that shows whether the game is started or not
        private bool gameStarted;
        public bool GameStarted
        {
            get
            {
                return gameStarted;
            }
            set
            {
                gameStarted = value;
            }
        }

        // The text that is displayed before the game starts
        [SerializeField]
        private GameObject startText;

        void Start()
        {
            // the game only starts when the player pressed the button for it
            Time.timeScale = 0;
            gameStarted = false;
        }

        // if the player presses the button, the game starts
        // the text will be deactivated
        public void Begin()
        {
            if(gameStarted == false)
            {
                Time.timeScale = 1;
                startText.SetActive(false);
                gameStarted = true;
            }

        }
    }
}
