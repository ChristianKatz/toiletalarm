﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// the script controlls the enemy
namespace Toilette
{
    public class Enemy : MonoBehaviour
    {

        // the position where the enemy moves down after he gets hit
        [SerializeField]
        private Transform moveDownDestination;

        // the position where the enemy moves up after he gets hit
        [SerializeField]
        private Transform moveTopDestination;

        // the seconds where the enemy waits to go up again
        [SerializeField]
        private float WaitSecondsToGoUpAgain;

        // the speed that determines how fast the enemy is moving up and down
        [SerializeField]
        private float movementSpeed;

        // bool value that shows if the enemy is at the top position
        private bool enemyMovesUp = true;

        // bool value that shows if the enemy is at the bottom position
        private bool enemyMovesDown;

        // show if the enemy has won or not
        private bool enemyHasWon = false;
        public bool EnemyHasWon
        {
            get
            {
                return enemyHasWon;
            }
            set
            {
                enemyHasWon = value;
            }
        }

        void Update()
        {
            // two methosd run in loop
            EnemyAppear();
            EnemyDisappear();
        }

        // if the enemy is hit by the toilet paper with the different tags, then the enemy goes down
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Left") || other.CompareTag("Right") || other.CompareTag("Toiletpaper"))
            {
                enemyMovesUp = false;
                enemyMovesDown = true;
            }

        }

        // if the enemy is allowed to go up and has reached his destination, the enemy has won
        private void EnemyAppear()
        {
            if (enemyMovesUp == true)
            {
                Vector3 TopPosition = Vector3.MoveTowards(transform.position, moveTopDestination.position, movementSpeed * Time.deltaTime);
                transform.position = TopPosition;

                if (Vector3.Distance(transform.position, moveTopDestination.position) < 0.1f)
                {
                    enemyHasWon = true;                   
                }
            }
        }

        // the seconds where the enemy is waiting to come up again
        IEnumerator EnemyMovement()
        {
            yield return new WaitForSeconds(WaitSecondsToGoUpAgain);
            enemyMovesUp = true;
        }

        // when the enemy is allowed to go down and has reached his destination the enemy is waiting to go up again
        private void EnemyDisappear()
        {
            if (enemyMovesDown == true)
            {
                Vector3 DownPosition = Vector3.MoveTowards(transform.position, moveDownDestination.position, movementSpeed * Time.deltaTime);
                transform.position = DownPosition;

                if (Vector3.Distance(transform.position, moveDownDestination.position) < 0.1f)
                {
                    StartCoroutine(EnemyMovement());
                    enemyMovesDown = false;
                }
            }
        }
    }
}