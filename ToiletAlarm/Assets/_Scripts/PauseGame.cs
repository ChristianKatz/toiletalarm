﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this script is for the possibility to pause the game
namespace Toilette
{
    public class PauseGame : MonoBehaviour
    {
        // varibale, which shows whether the pause menu is on or not
        private bool pause = false;

        // the text for the player, which button he has to press to get back into the game
        [SerializeField]
        private GameObject PauseText;

        // I need this script to know if the player has already started the game to activate the pause menu
        private StartGame startGame;

        // I need this script to know if the player has won the game to deactivate the pause menu
        private WinCondition winCondition;

	    void Start ()
        {
            // text should only appear if the player presses the button for the menu
            PauseText.SetActive(false);

            // get the scripts
            winCondition = FindObjectOfType<WinCondition>();
            startGame = FindObjectOfType<StartGame>();            
	    }

        // this method activates the pause menu
        // if the game is not finished or started, he is allowed to open the menu
        // the game will stop running if he paused and the text will appear
       public void PauseTheGame()
       {
            if(startGame.GameStarted == true && winCondition.Finished == false)
            {
                if (pause == false)
                {
                    PauseText.SetActive(true);
                    Time.timeScale = 0;
                    pause = true;

                }
                else
                {
                    PauseText.SetActive(false);
                    Time.timeScale = 1;
                    pause = false;
                }
            }
        }
    }
}
